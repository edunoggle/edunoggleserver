
import './App.css';

import About from './Components/About/About';
import Footer from './Components/Footer/Footer';
import Header from './Components/Header/Header';
import Home from './Components/Home/Home';
//Student-Parent Tabs
import StudentTab from './Components/StudentTab/Students';
import ParentTab from './Components/ParentTab/Parents';
import SubscribeTab from './Components/SubscribeTab/Subscribe';
import DiscussionBoard from './Components/DiscussionBoard/Board';

import StudentService from './Components/Login/login';
import AccountCreation from './Components/Login/register';
import Payment from './Components/Login/payment'

import AccountHome from './Components/AccountPage/AccountHome'
import AccountAbout from './Components/AccountPage/AccountAbout'
import AccountHeader from './Components/AccountPage/AccountHeader'
import AccountStudent from './Components/AccountPage/AccountStudents'
import AccountParent from './Components/AccountPage/AccountParents'
import AccountTeacher from './Components/AccountPage/AccountTeachers'
import AccountEducationLeaders from './Components/AccountPage/AccountEducationLeaders'
import Webinar from './Components/AccountPage/WebinarDisplay'
import Board from './Components/DiscussionBoard/DiscussionBoard'

import initializeAuthentication from './firebase/firebase.initialize';
import PageNotFound from './PageNotFound/PageNotFound';
import { GoogleAuthProvider , getAuth, signInWithPopup} from "firebase/auth";
import { BrowserRouter,Switch,Route } from 'react-router-dom';

initializeAuthentication();
const provider = new GoogleAuthProvider();

function App() {
        const  handleGoogleSignin=()=>{
        const auth = getAuth();
        signInWithPopup(auth, provider)
                .then((result) => {
                        const user = result.user;
                        console.log(user);
                })
        }
        return (
                <div className="">
                        <BrowserRouter>
                                <Header/>
                                <Switch>
                                        <Route exact path="/">
                                                <Home></Home>
                                        </Route>
                                        <Route path="/home">
                                                <Home></Home>
                                        </Route>
                                        <Route path="/students">
                                                <StudentTab></StudentTab>
                                        </Route>
                                        <Route path="/parents">
                                                <ParentTab></ParentTab>
                                        </Route>
                                        <Route path="/teachers">
                                                <DiscussionBoard></DiscussionBoard>
                                        </Route>
                                        <Route path="/about">
                                                <About></About>
                                        </Route>
                                        <Route path="/login">
                                                <StudentService></StudentService>
                                        </Route>
                                        <Route path="/register">
                                                <AccountCreation></AccountCreation>
                                        </Route>
                                        <Route path="/payment">
                                                <Payment></Payment>
                                        </Route>
                                        <Route path="/subscriptions">
                                                <SubscribeTab></SubscribeTab>
                                        </Route>
                                        {/* <Route path="*">
                                                <PageNotFound></PageNotFound>
                                        </Route> */}
                                        <Route path="/logged-in">
                                                <AccountHeader/>
                                                {/* <AccountHome></AccountHome> */}
                                                <Switch>
                                                        {/* <Route exact path="/">
                                                                
                                                        </Route> */}
                                                        <Route path="/logged-in/home">
                                                                <AccountHome></AccountHome>
                                                        </Route>
                                                        <Route path="/logged-in/about">
                                                                <AccountAbout></AccountAbout>
                                                        </Route>
                                                        <Route path="/logged-in/students">
                                                                <AccountStudent></AccountStudent>
                                                        </Route>
                                                        <Route path="/logged-in/parents">
                                                                <AccountParent></AccountParent>
                                                        </Route>
                                                        <Route path="/logged-in/educators">
                                                                <Board></Board>
                                                        </Route>
                                                        <Route path="/logged-in/education-leaders">
                                                                <AccountEducationLeaders></AccountEducationLeaders>
                                                        </Route>
                                                        <Route path = '/logged-in/webinar'>
                                                                <Webinar></Webinar>
                                                        </Route>
                                                </Switch>
                                        </Route>
                                </Switch>
                                <Footer/>
                        </BrowserRouter>
                </div>
        );
}

export default App;
