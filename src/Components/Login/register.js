import React from 'react';

import { getAuth, GoogleAuthProvider, signInWithPopup,GithubAuthProvider, signOut,createUserWithEmailAndPassword ,signInWithEmailAndPassword ,sendEmailVerification,sendPasswordResetEmail, updateProfile,FacebookAuthProvider } from "@firebase/auth";
import initializeAuthentication from "../../firebase/firebase.initialize";
import { useState } from "react";
import { NavLink } from "react-router-dom";

import learning from "../../Images/learning.png"

//-------------------- AuthenticatorAssertion initializeAuthentication---------------------
initializeAuthentication();

const provider = new GoogleAuthProvider();
const Gitprovider = new GithubAuthProvider();
const FacebookProvider = new FacebookAuthProvider();

const Register = () => {
  const [user,setuser]= useState({})

// ---------------state for email---------------
  const [email, setemail] = useState("");

// ---------------state for password---------------
  const [passWord, setpassWord] = useState("");
  const [error, seterror] = useState("");
  const [isLogin, setisLogin] = useState(false);
  const [name, setname] = useState("");

// ----------------------------namechange----------------------
  const handleNameChange=(e)=>{
    setname(e.target.value);
  }

  const setUsername=()=>{
    const auth = getAuth();
    updateProfile(auth.currentUser, {
      displayName: name, photoURL: "https://example.com/jane-q-user/profile.jpg"
    }).then(() => {
      // Profile updated!
      // ...
    }).catch((error) => {
      // An error occurred
      // ...
    });
  }

// --------------------------------------Google signin------------------------
  const  handleGoogleSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth, provider)
    .then((result) => {
      // const logInUser = result.user;
      // console.log(logInUser);
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };       
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      // Handle Errors here.
      // const errorCode = error.code;
      // const errorMessage = error.message;
      // // The email of the user's account used.
      // const email = error.email;
      // // The AuthCredential type that was used.
      // const credential = GoogleAuthProvider.credentialFromError(error);
      // // ...
      console.log(error.message);
    });
  }

// -----------------------------------------github signin------------------------
  const  handleGitSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth,Gitprovider)
    .then((result) => {
      // const logInUser = result.user;
      // console.log(logInUser);
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      console.log(error.message);
    });
  }

//------------------------------------  facebook signin--------------------------
  const handleFacebookSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth,FacebookProvider)
    .then((result) =>{
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      console.log(error.message);
    }); 
  }

 //-----------------------signout----------------------------------
  const  handleSignout=()=>{
    const auth = getAuth();
    signOut(auth).then(() => {
      setuser({});
    }).catch((error) => {
      // An error happened.
    });
  }

// ------------------------------------submit event handler for  email--------------------------
  const handleRegistration=(e)=>{
    e.preventDefault(); ///form by default reload hoi .ta bondo jorte preventDefault
    const auth = getAuth();
    if(passWord.length<6){
      seterror("Password must in 6 character long")
      return;
    }

    // --------------------------------case handle-------------------------------------
    if(!/(?=.*?[A-Z])/.test(passWord)){
      seterror("password must contain upper case");
      return;
    }

    const processLogin=(email,passWord)=>{
      const auth = getAuth();
      signInWithEmailAndPassword(auth,email,passWord)
      .then(result=>{
        const user=result.user;
        console.log(user);
        // seterror("");
      })
      .catch((error) => {
        seterror(error.message);
      });
    }
    
    const newUser=(email,passWord)=>{
      createUserWithEmailAndPassword(auth, email, passWord)
      .then((result) => {
            // Signed in 
            const user = result.user;
            console.log(user);
            seterror("");
            verifyEmail();
            setUsername();
          })
          .catch((error) => {
            console.log(error.message);
            // ..
          });
    }
    isLogin? processLogin(email,passWord):newUser(email,passWord);
  }

  const toggleLogin=(e)=>{
    setisLogin(e.target.checked);
  }

  // --------------------------------handle get email ------------------------
  const handleEmailChange=e=>{
    setemail(e.target.value);
  }

  // --------------------------------handle get password ------------------------
  const handlePassChange=e=>{
    // console.log(e.target.value);
    setpassWord(e.target.value);
  }

  const verifyEmail=()=>{
    const auth = getAuth();
  sendEmailVerification(auth.currentUser)
    .then((result) => {
      // Email verification sent!
      console.log(result);
    });
  }

  //-------------------------------------------------- password reset---------------------------
  const handleresetPassword=()=>{
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
    .then(() => {
      // Password reset email sent!
      // ..
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
    });
  }
  //Test to see if buttons for register roles work
  function sayStudent(){
    alert('Student');
  }
  function sayParent(){
    alert('Parent');
  }
  function sayEducator(){
    alert('Educator');
  }
     return (
          <div style={{backgroundColor: "lightblue"}}>
          <div className="container bg-white">
               <div  style={{marginTop:"148px", marginBottom: "20px", marginLeft: "-100px"}}>
                    <div className="row" >
                         <div className="col-6"> 
                              <div class="card card-fluid author py-5" style={{borderWidth: "3px", width: "90rem", height:"600px", borderColor: "coral"}}>
                                   <div className="d-flex">
                                        <div className="text-center" style={{marginTop:"0", marginLeft: "auto", marginRight: "0px"}}>
                                          <img src={learning} className="card-img-top " alt="..."/>
                                        </div>
                                        
                                        <div style={{marginTop:"35px", marginLeft: "auto", marginRight: "125px"}}>
                                           <h1 className="text-primary fs-1 fw-bold">Create an <span className="fs-1 text danger" style={{fontSize: "large"}}> Account</span></h1>

                                            <div class="input-group mb-3">
                                                <label style={{width: "69px"}} for="inputAddress" className="input-group-text" id="form-label">Name</label>
                                                <input type="text" onBlur={handleNameChange} class="form-control" id="inputAddress" placeholder=" Input your name"/>
                                            </div>
                                            <div className="input-group mb-3">
                                                <span className="input-group-text" id="basic-addon1">Email</span>
                                                <input type="text" className="form-control" placeholder="Input Email" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                            </div>
                                            <div className="input-group mb-3">
                                                <span className="input-group-text" id="basic-addon1">Username</span>
                                                <input type="text" className="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"/>
                                            </div>
                                            <div className="input-group mb-3">
                                                <span className="input-group-text" id="basic-addon1">Password</span>
                                                <input type="password" className="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1"/>
                                            </div>            
                                            <button type="button"><NavLink className="nav-link" to="/payment">Continue to payment</NavLink></button>
                                            {/* <button class="button button-grey button-border" type="button" className="btn btn-info me-5" style={{marginTop: "10px"}}><NavLink className="nav-link" to="/payment">Continue to payment</NavLink></button> */}
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div className="col-6"></div>
                    </div>
                    <div className="row">
                         <div className="col-6"></div>
                    </div>
               </div>
          </div>
          </div>
    );
};

export default Register;