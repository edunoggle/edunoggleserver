import React from 'react';
import { NavLink } from "react-router-dom";

import { getAuth, GoogleAuthProvider, signInWithPopup,GithubAuthProvider, signOut,createUserWithEmailAndPassword ,signInWithEmailAndPassword ,sendEmailVerification,sendPasswordResetEmail, updateProfile,FacebookAuthProvider } from "@firebase/auth";
import initializeAuthentication from "../../firebase/firebase.initialize";
import { useState } from "react";

//-------------------- AuthenticatorAssertion initializeAuthentication---------------------
initializeAuthentication();

const provider = new GoogleAuthProvider();
const Gitprovider = new GithubAuthProvider();
const FacebookProvider = new FacebookAuthProvider();

const Payment = () => {
  const [user,setuser]= useState({})

// ---------------state for email---------------
  const [email, setemail] = useState("");

// ---------------state for password---------------
  const [passWord, setpassWord] = useState("");
  const [error, seterror] = useState("");
  const [isLogin, setisLogin] = useState(false);
  const [name, setname] = useState("");

// ----------------------------namechange----------------------
  const handleNameChange=(e)=>{
    setname(e.target.value);
  }

  const setUsername=()=>{
    const auth = getAuth();
    updateProfile(auth.currentUser, {
      displayName: name, photoURL: "https://example.com/jane-q-user/profile.jpg"
    }).then(() => {
      // Profile updated!
      // ...
    }).catch((error) => {
      // An error occurred
      // ...
    });
  }

// --------------------------------------Google signin------------------------
  const  handleGoogleSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth, provider)
    .then((result) => {
      // const logInUser = result.user;
      // console.log(logInUser);
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };       
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      // Handle Errors here.
      // const errorCode = error.code;
      // const errorMessage = error.message;
      // // The email of the user's account used.
      // const email = error.email;
      // // The AuthCredential type that was used.
      // const credential = GoogleAuthProvider.credentialFromError(error);
      // // ...
      console.log(error.message);
    });
  }

// -----------------------------------------github signin------------------------
  const  handleGitSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth,Gitprovider)
    .then((result) => {
      // const logInUser = result.user;
      // console.log(logInUser);
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      console.log(error.message);
    });
  }

//------------------------------------  facebook signin--------------------------
  const handleFacebookSignin=()=>{
    const auth = getAuth();
    signInWithPopup(auth,FacebookProvider)
    .then((result) =>{
      const {displayName,email,photoURL} = result.user;
      const logInUser={
        name:displayName,
        email:email,
        photo:photoURL
      };
      console.log(logInUser);
      setuser(logInUser);
    })
    .catch((error) => {
      console.log(error.message);
    }); 
  }

 //-----------------------signout----------------------------------
  const  handleSignout=()=>{
    const auth = getAuth();
    signOut(auth).then(() => {
      setuser({});
    }).catch((error) => {
      // An error happened.
    });
  }

// ------------------------------------submit event handler for  email--------------------------
  const handleRegistration=(e)=>{
    e.preventDefault(); ///form by default reload hoi .ta bondo jorte preventDefault
    const auth = getAuth();
    if(passWord.length<6){
      seterror("Password must in 6 character long")
      return;
    }

    // --------------------------------case handle-------------------------------------
    if(!/(?=.*?[A-Z])/.test(passWord)){
      seterror("password must contain upper case");
      return;
    }

    const processLogin=(email,passWord)=>{
      const auth = getAuth();
      signInWithEmailAndPassword(auth,email,passWord)
      .then(result=>{
        const user=result.user;
        console.log(user);
        // seterror("");
      })
      .catch((error) => {
        seterror(error.message);
      });
    }
    
    const newUser=(email,passWord)=>{
      createUserWithEmailAndPassword(auth, email, passWord)
      .then((result) => {
            // Signed in 
            const user = result.user;
            console.log(user);
            seterror("");
            verifyEmail();
            setUsername();
          })
          .catch((error) => {
            console.log(error.message);
            // ..
          });
    }
    isLogin? processLogin(email,passWord):newUser(email,passWord);
  }

  const toggleLogin=(e)=>{
    setisLogin(e.target.checked);
  }

  // --------------------------------handle get email ------------------------
  const handleEmailChange=e=>{
    setemail(e.target.value);
  }

  // --------------------------------handle get password ------------------------
  const handlePassChange=e=>{
    // console.log(e.target.value);
    setpassWord(e.target.value);
  }

  const verifyEmail=()=>{
    const auth = getAuth();
  sendEmailVerification(auth.currentUser)
    .then((result) => {
      // Email verification sent!
      console.log(result);
    });
  }

  //-------------------------------------------------- password reset---------------------------
  const handleresetPassword=()=>{
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
    .then(() => {
      // Password reset email sent!
      // ..
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
    });
  }
  //Test to see if buttons for register roles work
  function sayStudent(){
    alert('Student subscription: $9.99');
  }
  function sayParent(){
    alert('Parent subscription: $x9.99');
  }
  function sayEducator(){
    alert('Educator subscription $x.xx');
  }
     return (
          <div style={{backgroundColor: "lightblue"}}>
          <div className="container bg-white">
               <div  style={{marginTop:"148px", marginBottom: "20px", marginLeft: "-100px"}}>
                    <div className="row" >
                         <div className="col-6"> 
                              <div class="card card-fluid author py-5" style={{borderWidth: "3px", width: "90rem", height:"600px", borderColor: "coral"}}>
                                   <div className="d-flex">
                                      <div style={{marginTop:"5px", marginLeft: "auto", marginRight: "75px"}}>
                                        <strong style={{fontSize: "xx-large"}}>View Available roles</strong>
                                        <div class="accordion" id="accordionExample" style={{width: "550px", marginTop: "15px"}}>
                                        <div class="accordion-item">
                                          <h2 class="accordion-header" id="headingOne">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              Students
                                            </button>
                                          </h2>
                                          <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                              This role offers worksheets, documents, and other additional resources to aid the student's learning experience.
                                              <p><strong>Grants access to the </strong></p> 
                                            </div>
                                          </div>
                                        </div>
                                        <div class="accordion-item">
                                          <h2 class="accordion-header" id="headingTwo">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Parents
                                            </button>
                                          </h2>
                                          <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                              This role offers documents, and other additional resources to aid parents in assisting their children through their curriculums in school.
                                              <p><strong>Grants access to specialized documents reserved for parents</strong></p> 
                                            </div>
                                          </div>
                                        </div>
                                        <div class="accordion-item">
                                          <h2 class="accordion-header" id="headingThree">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                              Teachers
                                            </button>
                                          </h2>
                                          <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                              This role offers a forum for teachers to be able to communicate and share tips in their field to help provide a better classroom environment.
                                              <p><strong>Grants access to a forum dedicated for teachers, as well as documents and learning activities.</strong></p> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      </div>
                                        <div style={{marginTop:"5px", marginLeft: "75px", marginRight: "auto"}}>
                                          <div style={{marginBottom: "25px"}}>
                                            <h1 className="text-primary fs-1 fw-bold" style={{fontSize: "xx-large"}}>Select an account type</h1>
                                            <button onClick = {sayStudent} className="btn btn-danger" type="submit" style={{width: "150px", backgroundColor: "green", color: "white", borderColor: "green"}}>Student</button>
                                            <button onClick = {sayParent} className="btn btn-danger" type="submit" style={{marginLeft: "10px", marginRight: "10px", width: "150px", backgroundColor: "green", color: "white", borderColor: "green"}}>Parent</button>
                                            <button onClick = {sayEducator} className="btn btn-danger" type="submit" style={{width: "150px", backgroundColor: "green", color: "white", borderColor: "green"}}>Educator</button>
                                          </div>
                                          <div class="input-group mb-3">
                                              <span className="input-group-text" id="basic-addon1">Card Number</span>
                                              <input type="text" onBlur={handleNameChange} class="form-control" id="inputAddress" placeholder="xxxx-xxxx-xxxx-xxxx"/>
                                          </div>
                                          <div className="input-group mb-3">
                                              <span className="input-group-text" id="basic-addon1">Card Name</span>
                                              <input type="text" className="form-control" placeholder="Input Name" aria-label="Cardholder's name" aria-describedby="basic-addon2"/>
                                          </div>
                                          <div className="input-group mb-3">
                                              <span className="input-group-text" id="basic-addon1">Expiration</span>
                                              <input type="text" className="form-control" placeholder="MM/DD" aria-label="Card's expiration" aria-describedby="basic-addon1"/>
                                              <span className="input-group-text" id="basic-addon1">CVC</span>
                                              <input type="text" className="form-control" placeholder="xxx" aria-label="Card's Verification Code" aria-describedby="basic-addon1"/>
                                          </div>
                                          <div></div>
                                          <div className="input-group mb-3">
                                              <span className="input-group-text" id="basic-addon1">Address</span>
                                              <input type="text" className="form-control" placeholder="Input Billing Address" aria-label="Billing Address" aria-describedby="basic-addon2"/>
                                          </div>
                                          <div className="input-group mb-3">
                                              <span className="input-group-text" id="basic-addon1">City</span>
                                              <input type="text" className="form-control" placeholder="City" aria-label="Billing City" aria-describedby="basic-addon2"/>
                                              <span className="input-group-text" id="basic-addon1">State</span>
                                              <input type="text" className="form-control" placeholder="State" aria-label="Billing City" aria-describedby="basic-addon2"/>
                                          </div>        
                                          <div className="input-group mb-3">
                                            <span className="input-group-text" id="basic-addon1">Zip Code</span>
                                            <input type="text" className="form-control" placeholder="" aria-label="Billing City" aria-describedby="basic-addon2"/>
                                          </div>
                                          <button type="button"><NavLink className="nav-link" to="/logged-in/home">Register</NavLink></button>  
                                          {/* <button type="button" className="btn btn-info me-5" style={{marginTop: "10px", backgroundColor: "lightblue", color: "white", borderColor: "lightblue", color: "black"}}><NavLink className="nav-link" to="/logged-in/home">Register</NavLink></button> */}
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div className="col-6"></div>
                    </div>
                    <div className="row">
                         <div className="col-6"></div>
                    </div>
               </div>
          </div>
          </div>
    );
};

export default Payment;