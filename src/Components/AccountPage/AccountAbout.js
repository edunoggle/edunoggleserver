import React from 'react';
import "../About/About.css"

import one from "../../Images/author/Jannell-Campbell_1.PNG"

const About = () => {
     return (
          <div style={{backgroundColor: "lightblue"}}>
          <div className="container bg-white">
               <div  style={{marginTop:"148px", marginBottom: "20px", marginLeft: "-100px"}}>
                    <div className="row" >
                         <div className="col-6"> 
                              <div class="card card-fluid author py-5" style={{borderWidth: "3px", width: "90rem", height:"600px", borderColor: "coral"}}>
                                   <div className="d-flex">
                                        <img style={{width: "18rem", height: "200px", marginLeft: "15px"}} src={one} className="card-img-top " alt="..."/>
                                         <div class="card-body" style={{marginLeft: "15px"}}>
                                             <h5 class="card-title">Meet Jannell Pearson-Campbell</h5>
                                             <p class="card-text">
                                                  Dr. Jannell Pearson-Campbell is a 20-year educational leader and practitioner, with a career dedicated to serving diverse learners. Dr. Pearson-Campbell has been a teacher and administrative leader in urban and suburban districts where she has built the capacity of leaders in diversity, equity, and inclusion. 
                                             </p>
                                             <p class="card-text">
                                                 Drawing from her experience in teaching math, science, English language instruction, and Special Education in Boston and Framingham Public Schools, Dr. Pearson-Campbell has been successful in using data, protocols, and policies to drive instruction for all students.  By strategically integrating Science, Technology, Engineering, Mathematics (STEM), and social-emotional learning (SEL) in schools, Dr. Pearson-Campbell has been instrumental in helping districts impact student achievement and outcomes. Additionally, as a part of the Accelerated Improvement Plan, Dr. Pearson-Campbell created an induction program focused on Common Core Standards, Data Management, and using online standards-based lessons to drive differentiated instruction for students. Through strategic analysis and planning, Dr. Pearson-Campbell provides opportunities for educators and administrators to develop a reflective lens in their practice, to make an everlasting change in the school district. 
                                             </p>
                                             <p class="card-text">
                                               Dr. Pearson-Campbell earned her Bachelor’s degree at Florida A & M University,  Master’s of Special Education at the University of Massachusetts - Boston, and C.A.G.S. in Educational Leadership at Bridgewater State University. In the Spring of 2019, Dr. Pearson-Campbell completed her doctoral program at the University of Massachusetts - Lowell, where her dissertation examined the special education pre-referral process for culturally and linguistically diverse students. 
                                             </p>
                                             <p class="card-text">
                                             Currently, Dr. Pearson-Campbell is Assistant Superintendent of Teaching and Learning for Old Rochester Regional School District, a district in southeast Massachusetts. 
                                             </p>
                                             <p class="card-text">
                                                 Past leadership roles include Dr. Pearson-Campbell serving as an Assistant Principal at Hanover Middle School (5-8) in Hanover, Massachusetts, where she developed new 21st Century Skills activities focused on integrating science, technology, engineering, and mathematics. Additionally, Dr. Pearson-Campbell worked as an Elementary Turn-Around Principal (K-5) in New Bedford, Massachusetts. 
                                             </p>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div className="col-6"></div>
                    </div>
                    <div className="row">
                         <div className="col-6"></div>
                    </div>
               </div>
          </div>
          </div>
    );
};

export default About;
















