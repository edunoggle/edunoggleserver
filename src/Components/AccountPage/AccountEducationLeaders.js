import React from 'react';
import { useState, useEffect } from "react";

import Standards from "../EduLeadersTab/Standards";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleRight } from "@fortawesome/free-regular-svg-icons";

const AccountEducationLeaders = () => {
    const [Standardsdata, setStandardsData] = useState([]);

    useEffect(() => {
        fetch("../EduLeadersJSON/FakeStandards.JSON")
        .then(res=>res.json())
        .then(data=>setStandardsData(data))
    }, [])
   
    const handler=()=>{
        window.location.reload();
        // history.push("/services")
        console.log("here")
    }
    return (
        <div className="bg-info" style={{}}>
            <div classname="container" style={{marginTop:"125px", marginRight:"-100px", backgroundColor: "lightBlue", paddingBottom:"15px"}}>
                <div>
                    {/* <Link to={"/MathematicsCourse" } style={{textDecoration:"none"}}> */}
                    <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>Standards </h1>
                    {/* </Link> */}
                    <div style={{marginTop: "35px",marginRight:"225px"}}>
                        <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                            Standardsdata.map(item=><Standards
                            key={item.id}
                            standards={item}
                            ></Standards>)
                        }
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    );
};

export default AccountEducationLeaders;