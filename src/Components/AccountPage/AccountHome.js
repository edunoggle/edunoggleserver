import React, { useEffect, useState } from 'react';
import HomePageAboutUs from "./AccountAboutUs"
import Title from "./AccountTitle";
import { useHistory } from "react-router";


const AccountPage = () => {
    const [data, setdata] = useState([]);
    useEffect(() => {
        fetch("../FakeData.JSON")
        .then(res=>res.json())
        .then(data=>setdata(data))
    }, [])
    const history=useHistory();
    const useHandler=()=>{
        history.push("/services");
    }
    console.log(data);
    //----------------------- main home component where contain three child component-------------------
    const getData=data.slice(0,5);
    return (
        <>
            <div className="overflow-hidden">
                <Title></Title>
                <HomePageAboutUs></HomePageAboutUs>
                {/* <Instructor></Instructor> */}
            </div>    
        </>
    );
};

export default AccountPage;