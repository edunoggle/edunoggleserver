import React from 'react';
import { useState, useEffect } from "react";
import ParentCards from './ParentCards';

const AccountParent = () => {
    const [Parentdata, setParentdata] = useState([]);

    useEffect(() => {
        fetch("../ParentJSON/FakeAuthority.JSON")
        .then(res=>res.json())
        .then(data=>setParentdata(data))
    }, [])
    const handler=()=>{
        window.location.reload();
        // history.push("/services")
        console.log("here")
    }
    return (
        <div classname="container" style={{marginTop: "125px", backgroundColor: "lightBlue", height: "auto", paddingBottom:"50px"}}>
            <div>
                <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>Curriculums</h1>
                <div style={{marginTop: "35px",marginRight:"225px", marginBottom:"15px"}}>
                    <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                        Parentdata.map(item=><ParentCards
                        key={item.id}
                        parent={item}
                        ></ParentCards>)
                    }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AccountParent;