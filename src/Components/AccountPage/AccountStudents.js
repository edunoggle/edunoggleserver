import React from 'react';
import { useState, useEffect } from "react";

import Math from "../StudentTab/Math";
import History from "../StudentTab/History";
import Biology from "../StudentTab/Biology";
import English from "../StudentTab/English";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleRight } from "@fortawesome/free-regular-svg-icons";

const AccountStudents = () => {
    const [Mathdata, setMathdata] = useState([]);
    const [BioData, setBioData] = useState([]);
    const [Histdata, setHistData] = useState([]);
    const [Engdata, setEngData] = useState([]);
    const [data, setdata] = useState([]);

    useEffect(() => {
        fetch("../StudentJSON/FakeData.JSON")
        .then(res=>res.json())
        .then(data=>setdata(data))
    }, [])
    useEffect(() => {
        fetch("../StudentJSON/FakeMath.JSON")
        .then(res=>res.json())
        .then(data=>setMathdata(data))
    }, [])
    useEffect(() => {
        fetch("../StudentJSON/FakeHistory.JSON")
        .then(res=>res.json())
        .then(data=>setHistData(data))
    }, [])
    useEffect(() => {
        fetch("../StudentJSON/FakeBiology.JSON")
        .then(res=>res.json())
        .then(data=>setBioData(data))
    }, [])
    useEffect(() => {
        fetch("../StudentJSON/FakeEnglish.JSON")
        .then(res=>res.json())
        .then(data=>setEngData(data))
    }, [])
    const handler=()=>{
        window.location.reload();
        // history.push("/services")
        console.log("here")
    }
    return (
        <div className="bg-info" style={{}}>
            <div classname="container" style={{marginTop:"125px", marginRight:"-100px", backgroundColor: "lightBlue", paddingBottom:"15px"}}>
                <div>
                    {/* <Link to={"/MathematicsCourse" } style={{textDecoration:"none"}}> */}
                    <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>Mathematics <FontAwesomeIcon icon ={faArrowAltCircleRight} ></FontAwesomeIcon></h1>
                    {/* </Link> */}
                    <div style={{marginTop: "35px",marginRight:"225px"}}>
                        <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                            Mathdata.map(item=><Math
                            key={item.id}
                            math={item}
                            ></Math>)
                        }
                        </div>
                    </div>
                </div>
            </div>
            <div classname="container" style={{marginRight:"-100px", backgroundColor: "lightBlue"}}>
                <div>
                    {/* <Link to={"/HistoryCourse" } style={{textDecoration:"none"}}> */}
                    <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>History <FontAwesomeIcon icon ={faArrowAltCircleRight}></FontAwesomeIcon></h1>
                    {/* </Link> */}
                    <div style={{marginTop: "35px",marginRight:"225px"}}>
                        <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                            Histdata.map(item=><History
                            key={item.id}
                            hist={item}
                            ></History>)
                        }
                        </div>
                    </div>
                </div>
            </div>
            <div classname="container" style={{marginRight:"-100px", backgroundColor: "lightBlue"}}>
                <div>
                    <h1 className="border p-4 bg-Secondary fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>Biology <FontAwesomeIcon icon ={faArrowAltCircleRight} style={{color: "#151441"}}></FontAwesomeIcon>
                    </h1>
                    <div style={{marginTop: "35px",marginRight:"225px"}}>
                        <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                            BioData.map(item=><Biology
                            key={item.id}
                            bio={item}
                            ></Biology>)
                        }
                        </div>
                    </div>
                </div>
            </div>
            <div classname="container" style={{marginRight:"-100px", backgroundColor: "lightBlue"}}>
                <div>
                    {/* <Link to={"/HistoryCourse" } style={{textDecoration:"none"}}> */}
                        <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>English <FontAwesomeIcon icon ={faArrowAltCircleRight}></FontAwesomeIcon>
                        </h1>
                    {/* </Link> */}
                    <div style={{marginTop: "35px",marginRight:"225px"}}>
                        <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                            Engdata.map(item=><English
                            key={item.id}
                            eng={item}
                            ></English>)
                        }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AccountStudents;