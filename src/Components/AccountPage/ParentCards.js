import React from 'react';

const ParentCards = (props) => {
  const {img,name,Instructor,desc,price}=props.parent;
  console.log(props);
  
  return (
    <>
      <div className="col-lg-4 col-sm-3 col-12">
        <div className="card hover" style={{height:"420px",borderRadius:"30px"}}   >
          <img  height="200px" src={img} className="card-img-top" alt="..."/>
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <div className="d-flex justify-content-between">
              <button type="button" className="btn btn-danger ms-3">VIEW</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ParentCards;