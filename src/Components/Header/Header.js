import "./Header.css"
import "animate.css"


import React from 'react';
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookOpen } from "@fortawesome/free-solid-svg-icons";

const Header = () => {
  const activeStyle = {
      fontWeight: "bold",
      color: "tomato"    
  }
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark primary fixed-top " style={{backgroundColor: "#151441"}}>
        <div className="container-fluid">
          <a className="navbar-brand ms-5" style={{fontSize:"25px"}} href="/Home"><span className="focusName text-danger fw-bolder">Edu</span>Noggle <FontAwesomeIcon className="ms-2" style={{fontSize:"40px"}} icon={faBookOpen}></FontAwesomeIcon></a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent"  style={{marginTop: "10px"}}>
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink activeStyle={activeStyle} className="nav-link active" aria-current="page" to="/Home">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeStyle={activeStyle}  className="nav-link" to="/About">About us</NavLink>
              </li>
              <li className="nav-item" style={{marginTop: "-10px"}}>
                <NavLink  activeStyle={activeStyle} className="nav-link" to="/Subscriptions">
                  <button className="btn btn-danger" type="submit" style={{backgroundColor: "green", color: "white", borderColor: "green"}}>Subscribe</button>
                </NavLink>
              </li>
              <li className="nav-item" style={{marginTop: "-10px"}}>
                <NavLink  activeStyle={activeStyle} className="nav-link" to="/Login"><button className="btn btn-danger" type="submit">Log In</button></NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {/* -----------------------nav bar end------------------------------------------------ */}
    </>
  );
};

export default Header;