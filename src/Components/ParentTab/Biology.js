import React from 'react';

const Biology = (props) => {
  const {img,name,Instructor,rating,price,desc}=props.bio;
  console.log(props);

  return (
    <>
      <div className="col-lg-4 col-sm-3 col-12">
        <div className="card hover" style={{height:"350px",borderRadius:"30px"}}   >
          <img  height="200px" src={img} className="card-img-top" alt="..."/>
          <div className="card-body">
            <h5 className="card-title">{name}<button type="button" className="btn btn-danger ms-3">VIEW</button></h5>
          </div>
        </div>
      </div>
    </>
  );
};

export default Biology;