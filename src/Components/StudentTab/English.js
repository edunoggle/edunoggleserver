import React from 'react';

const English = (props) => {
  const {img,name,Instructor,rating,price,desc}=props.eng;
  console.log(props);

  return (
    <>
      <div className="col-lg-4 col-sm-3 col-12">
        <div className="card hover" style={{height:"350px",borderRadius:"30px"}}   >
          <img  height="200px" src={img} className="card-img-top" alt="..."/>
          <div className="card-body">
            <h5 className="card-title">{name}<button type="button" className="btn btn-danger ms-3"><a href = "https://first-english.org/english_learning/english_tenses/simple_present_worksheets/simple_present_03_worksheet.pdf" style={{color:"White"}}>VIEW</a></button></h5>
          </div>
        </div>
      </div>
    </>
  );
};

export default English;