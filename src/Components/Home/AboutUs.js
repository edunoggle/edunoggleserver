import React from 'react'
import { useHistory } from "react-router";

export default function AboutUs() {
    const history = useHistory();
    const handleClick = (link) => {
        history.push(link);
    }

    return (
        <div className="mb-5 mt-5">   
            <div className="d-lg-flex  align-items-baseline justify-content-around" style={{backgroundColor: "coral"}}>
                <div>
                    <div className="header ">
                        <h1  className="Title animate__animated animate__heartBeat">EduNoggle's Misson</h1>
                    </div>
                    <div>
                        <p className="p-2 text-center" style={{marginLeft: "200px", marginRight: "200px"}}>
                            {/* <large style={{color: "#151441"}}> */}
                            Since 2000, Dr. Jannell Pearson- Campbell has addressed the achievement gap for diverse students, schools, and developed plans to prepare them for the future.
                                EduNoggle was created as an educational consulting firm by constantly bridging the gap during the pandemic. As a reflective administrator, she has created opportunities 
                                for educators who needed support in Diversity, Equity, and Inclusion to begin to examine the barriers of preparing all students for college and career. While at the same time 
                                propel the sense of excellence expiring all students to think beyond themselves and see the possibility of what can happen when people work together. She has taught, led, and provided one-to-one support on preparing students of English Language Learner, Students with Disabilities, and preparing for STEAM. Her mission is to be the connector of the community. As an African-American educator, Dr. Pearson-Campbell has benefited from having role models and mentors who supported and prepared her for community leadership.
                            {/* </large> */}
                        </p>
                    </div>
                    <div className="text-center mb-5">
                        <button variant="primary" type="button" size="sm" style={{borderRadius:"10px"}} className="btn btn-danger me-3 fs-3 px-4 " onClick={()=>handleClick("/about")}> Learn more</button>
                    </div>
                </div>
            </div>
        </div>            
    )
}
