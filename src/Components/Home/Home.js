import "./Home.css"

import React, { useEffect, useState } from 'react';
import HomePageAboutUs from "./AboutUs"
import Title from "../Title/Title";
import { useHistory } from "react-router";


const Home = () => {
    const [data, setdata] = useState([]);
    useEffect(() => {
        fetch("./FakeData.JSON")
        .then(res=>res.json())
        .then(data=>setdata(data))
    }, [])
    const history=useHistory();
    const useHandler=()=>{
        history.push("/services");
    }
    console.log(data);
    //----------------------- main home component where contain three child component-------------------
    const getData=data.slice(0,5);
    return (
        <>
            <div className="overflow-hidden">
                <Title></Title>
                <HomePageAboutUs></HomePageAboutUs>
            </div>    
        </>
    );
};

export default Home;