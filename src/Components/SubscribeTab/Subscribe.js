import React from 'react';
import { useState, useEffect } from "react";

import Subscriptions from '../Subscriptions/Subscriptions';

const Subscribe = () => {
    const [Subdata, setSubdata] = useState([]);

    useEffect(() => {
        fetch("./FakeSub.JSON")
        .then(res=>res.json())
        .then(data=>setSubdata(data))
    }, [])
    const handler=()=>{
        window.location.reload();
        // history.push("/services")
        console.log("here")
    }
    return (
        <div classname="container" style={{marginTop: "125px", backgroundColor: "lightBlue", height: "642px"}}>
            <div>
                <h1 className="border p-4 text-dark fw-bolder mt-5 p-5" style={{backgroundColor: "coral", color: "#151441"}}>Subscription Types</h1>
                <div style={{marginTop: "35px",marginRight:"225px"}}>
                    <div class="row row-cols-1 row-cols-md-4 g-4" style={{marginLeft:"225px"}}>{
                        Subdata.map(item=><Subscriptions
                        key={item.id}
                        sub={item}
                        ></Subscriptions>)
                    }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Subscribe;