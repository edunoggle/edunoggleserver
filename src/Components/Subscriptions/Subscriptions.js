import React from 'react';
import { NavLink } from "react-router-dom";

const Subscriptions = (props) => {
  const {img,name,Instructor,desc,price}=props.sub;
  console.log(props);
  
  return (
    <>
      <div className="col-lg-4 col-sm-3 col-12">
        <div className="card hover" style={{height:"420px",borderRadius:"30px"}}   >
          <img  height="200px" src={img} className="card-img-top" alt="..."/>
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            {/* <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
            <div className="d-flex justify-content-between">
              {/* <div className="d-flex"> */}
                <div>
                  <h5>{desc}</h5>
                </div>
              <button type="submit" style= {{backgroundColor: "green", borderColor: "lightgreen"}}><NavLink className="nav-link" to="/register" style={{color: "white", backgroundColor: "green"}}>VIEW</NavLink></button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Subscriptions;